package controller;

import marta.tfg.machinelearning.martatfg.executors.PredictionExecution;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PredictionControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PredictionExecution pEx;

    @Test
    public void linearRegression( ) {
        this.webTestClient.get().uri("/prediction/linearRegression?tableName=test&fields=id,name,age")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo("{\"id\":\"1\",\"name\":\"Marta\",\"age\":\"20\"}");
    }

}

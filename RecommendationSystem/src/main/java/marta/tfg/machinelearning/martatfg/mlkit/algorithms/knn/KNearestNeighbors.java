package marta.tfg.machinelearning.martatfg.mlkit.algorithms.knn;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.IAlgorithm;
import java.util.*;
import java.util.Map.Entry;

/**
 * 
 * @author martafernandezgarcia
 *
 */

public class KNearestNeighbors implements IAlgorithm {

	private String type;
	/**
	 * These are the known values. We know which class they belong to.
	 * Neighbour + weight
	 */
	private HashMap<Neighbour, Double> trainingPoints;
	/**
	 * This is the value whose class is unknown. 
	 */
	private Neighbour testPoint;
	public static int K = 3;

	/**
	 * Calculates the distance between to points and updates this distance in the trainingPoints structure
	 * @param point
	 */
	private void distanceBetweenPoints(Neighbour point) {
		double distance = this.testPoint.calculateDistance(point);
		point.setDistance(distance);
		this.trainingPoints.put(point, distance);
	}
	
	/**
	 * 
	 * @param classLabels input classLabels
	 * @return the most repeated in the collection
	 */
	private String mostFrequentClass(Collection<String> classLabels) {
		TreeMap<String, Integer> frequencies = new TreeMap<>();
		int count, maxCount = 0;
			
		for(String label: classLabels) {
			if ( !frequencies.containsKey(label) ) frequencies.put(label, 1);
			else {
				count = frequencies.get(label);
				count ++;
				if ( count > maxCount ) maxCount = count;
				frequencies.put(label, count);
			}
		}
		
		for(Entry<String, Integer> entry : frequencies.entrySet()) {
			if ( entry.getValue() == maxCount ) return entry.getKey();
		}
				
		return "undefined";
	}

	public String kNearesNeighbors( HashMap<Neighbour, Double> trainingPoints, Neighbour testPoint, String type ) {
		this.trainingPoints = new HashMap<>(trainingPoints);
		this.testPoint = testPoint;
		this.type = type;

		for (Neighbour n : this.trainingPoints.keySet()) { this.distanceBetweenPoints(n); }
		LinkedList<String> topK = new LinkedList<>();
		ArrayList<Neighbour> orderedList = new ArrayList<>(this.trainingPoints.keySet());
		Collections.sort(orderedList);
		String result = "";
		int count = 0;

		for (Neighbour n : orderedList) {
			if ( count == K ) break;
			topK.add(n.getClassLabel());
			count ++;
		}

		// If we want to predict we will take the k most similar points and select the most repeated class
		if (type.equals("prediction")) {
			if ( topK.size() <= 2)  result = topK.get(0);
			else result = this.mostFrequentClass(topK);
		}

		// If we want to get the most similar user, we will simply return the first element from the ordered list
		if (type.equals("supervised")) result = orderedList.get(0).getClassLabel();

		return result;
	}

	@Override
	public String executeWithTestData(Object testData) { return null; }
}

package marta.tfg.machinelearning.martatfg.mlkit.algorithms.linearregression;

import marta.tfg.machinelearning.martatfg.mlkit.algorithms.IAlgorithm;
import marta.tfg.machinelearning.martatfg.statistics.Statistics;
import java.util.ArrayList;

/**
 * @author Marta Fernández García
 */
public class LinearRegression implements IAlgorithm {

	private double a = .0;
	private double b = .0;
	private static double L = 0.005;
	private static int nMaxIter = 100000;
	private final ArrayList<Double> x;
	private final ArrayList<Double> y;

	/**
	 * 
	 * @param x independent distribution
	 * @param y dependent distribution
	 */
	public LinearRegression(ArrayList<Double> x, ArrayList<Double> y) throws Exception {
		this.x = x;
		this.y = y;
		if(!this.isLinear(x, y)) { throw new Exception("This function does not follow a linear distribution"); }
	}

	/**
	 * 
	 * @param x independent distribution
	 * @param y dependent distribution
	 * @return true if it follows a linear distribution
	 * @throws Exception if both lists don't have the same size
	 */
	private boolean isLinear(ArrayList<Double> x, ArrayList<Double> y) throws Exception {
		return Math.abs(Math.round(Statistics.pearsonCoefficient(x, y))) == 1 ;
	}

	/**
	 * 
	 * @param x independent value
	 * @return model result
	 */
	private double hypothesisFunction(Double x) {
		return a * x + b;
	}

	/**
	 * 
	 * @param x independent value
	 * @param y dependent value
	 * @return error
	 */
	private double errorFunction(Double x, Double y) {
		return y-hypothesisFunction(x);
	}

	private void gradientDescent() {
		double sum0 = 0.0;
		double sum1 = 0.0;

		for (int i = 0; i < x.size(); i++) {
			sum0 += x.get(i) * errorFunction(x.get(i), y.get(i));
			sum1 += errorFunction(x.get(i), y.get(i));			
		}

		sum0 *= (-2/(x.size()*1.0));
		sum1 *= (-2/(x.size()*1.0));

		this.a = a - (L * sum0);
		this.b = b - (L * sum1);
	}

	public void linearRegression() {
		this.a = .0;
		this.b = .0;
		int iter = 0;
		while (iter <= nMaxIter) {
			gradientDescent();
			iter++;
		}
	}

	/**
	 * 
	 * @return a parameter
	 */
	public double getA() {
		return a;
	}
	/**
	 * 
	 * @return  b parameter
	 */
	public double getB() {
		return b;
	}

	/**
	 *
	 * @param testData independent value
	 * @return model result
	 */
	@Override
	public String executeWithTestData(Object testData) {
		Double xTestData = (Double)testData;
		return Double.toString(this.hypothesisFunction(xTestData));
	}
}

package marta.tfg.machinelearning.martatfg.mlkit.algorithms.kmeans;

import marta.tfg.machinelearning.martatfg.metrics.LabelEncoder;
import marta.tfg.machinelearning.martatfg.metrics.Metrics;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.IAlgorithm;
import marta.tfg.machinelearning.martatfg.model.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * @author Marta Fernández García
 */
@Component
public class KMeans implements IAlgorithm {
    private static int K = 3; // Number of clusters
    private String tableName;
    private ArrayList<Model> data;
    public static final Random random = new Random();
    private Set<Cluster> centroids;
    @Autowired
    LabelEncoder lEncoder;

    /**
     *
     * @param tableName database table name
     * @param data the database table data
     */
    public void initialize( String tableName, ArrayList<Model> data ) {
        this.tableName = tableName;
        this.data = data;
        this.centroids = new HashSet<>();
        this.setRandomCentroids();
    }

    /**
     * Establish random centroids
     */
    private void setRandomCentroids() {
        double newCenter;
        int sum = 0, count = 0;
        boolean isClose;

        while ( count != K ) {
            newCenter = (100)*random .nextDouble() + sum;
            isClose = this.checkIfClusterIsCloseToTheOthers( newCenter );
            if ( !isClose ) {
                this.centroids.add( new Cluster( newCenter ) );
                count ++;
            } else { sum += 5; }
        }
    }

    /**
     *
     * @param newCenter center to check if is too close to other in the centroids set
     * @return true if it is too cloe
     */
    private boolean checkIfClusterIsCloseToTheOthers( Double newCenter ) {
        double distance;
        if ( this.centroids == null ) { return false; }
        for ( Cluster cluster : centroids ) {
            distance = this.calculateDistanceBetweenClusters( newCenter, cluster );
            if ( distance < 20 ) return true;
        }
        return false;
    }

    /**
     *
     * @param newCenter new center to calculate the distance with
     * @param c2 cluster to calculate the distance to
     * @return the distance between the new center and the cluster
     */
    private double calculateDistanceBetweenClusters( Double newCenter, Cluster c2 ) {
        return Math.abs( newCenter -  c2.getCenter() );
    }

    /**
     *
     * @param c cluster
     * @param m model
     * @return the distance between the cluster and the model
     */
    private double calculateDistanceToCluster( Cluster c, Model m ) {
        double auxDistance, distance = .0;

        for ( Map.Entry<String, String> attributes : m.getAttributes().entrySet() ) {
            if ( this.isDataNumerical(attributes.getValue())) {
                distance += Metrics.absDifference( c.getCenter(), Double.parseDouble( attributes.getValue() ) );
            } else {
                auxDistance = this.lEncoder.getValueByProperty(attributes.getKey(), attributes.getValue());
                distance += Metrics.absDifference( c.getCenter(),auxDistance );
            }
        }
        return distance;
    }

    /**
     *
     * @param data to check if it is numerical
     * @return true if it is numerical
     */
    private boolean isDataNumerical ( String data ) {
        try { Double.parseDouble( data ); }
        catch ( Exception exception ) { return false; }
        return true;
    }


    private void initializeLabelEncoder() {
        Model m = this.data.get(0);
        for ( Map.Entry<String, String> attributes : m.getAttributes().entrySet() ) {
            try { Double.parseDouble(attributes.getValue()); }
            catch ( Exception exception ) { this.lEncoder.labelEncoder(tableName, attributes.getKey()); }
        }
    }

    /**
     * Establish the model database data to the closest cluster
     */
    private void setDataToTheClosestCluster() {
        Map<Cluster, Double> distanceToCluster;
        for ( Model m : this.data ) {
            distanceToCluster = new TreeMap<>();
            for ( Cluster c : this.centroids ) distanceToCluster.put(c,this.calculateDistanceToCluster( c,m ));
            Double minDistance = distanceToCluster.values().stream().min(Double::compare).get();
            Cluster c = this.getClusterGivenDistance( distanceToCluster, minDistance );
            this.addDataToCluster(c, m);
        }
    }

    /**
     *
     * @param distancesToCluster all the entries with cluster and distance
     * @param distance the distance for the cluster
     * @return the cluster given the distance
     */
    private Cluster getClusterGivenDistance(Map<Cluster, Double> distancesToCluster, Double distance) {
        Set<Cluster> clusters = distancesToCluster.keySet();
        for ( Cluster c : clusters ) { if ( distancesToCluster.get( c ).equals( distance ) ) return c; }
        return null;
    }

    /**
     *
     * @param cluster containing the new data
     * @param data to add in the cluster
     */
    private void addDataToCluster(Cluster cluster, Model data ) {
        for ( Cluster c : this.centroids ) if ( c.equals(cluster) ) c.addData( data );
    }

    /**
     * Calculates the mean of the cluster
     * @param c cluster to calculate the mean
     * @return the mean
     */
    private double calculateMeanInClusterData(Cluster c) {
        double average = .0, count = .0, value;

        for ( Model m : c.getData() ) {
            for ( Map.Entry<String, String> attributes : m.getAttributes().entrySet() ) {
                try { value = Double.parseDouble( attributes.getValue() ); }
                catch ( Exception exception ) { value = this.lEncoder.getValueByProperty(attributes.getKey(), attributes.getValue()); }
                average += value;
                count ++;
            }
        }

        return (average/count);
    }

    /**
     *
     * @param clusterMeans given the cluster with their means
     * @return relocation
     */
    private boolean relocateCentroids(TreeMap<Cluster, Double> clusterMeans) {
        boolean relocating = false;
        double difference = 10;
        for ( Map.Entry<Cluster, Double> clusterMean : clusterMeans.entrySet() ) {
            if ( Math.abs(clusterMean.getValue()  - clusterMean.getKey().getCenter() ) > difference) {
                clusterMean.getKey().setCenter( clusterMean.getValue() );
                relocating = true;
            }
        }

        return relocating;
    }

    /**
     *
     * @param item the model to find which cluster belongs to
     * @return the model's cluster
     */
    public Cluster findClusterByItem(Model item) {
        for ( Cluster c : this.centroids ) {
            for ( Model m : c.getData() ) {
                if ( m.equals(item) ) return c;
            }
        }
        return null;
    }

    /**
     *
     * @return executes the kmeans algorithm
     */
    public Set<Cluster> kmeans() {
        TreeMap<Cluster, Double> clusterMeans;
        boolean relocating = true;
        this.lEncoder.initialize();
        this.initializeLabelEncoder();

        while ( relocating ) {
            this.setDataToTheClosestCluster();
            clusterMeans = new TreeMap<>();

            for ( Cluster c : this.centroids ) { clusterMeans.put(c,  this.calculateMeanInClusterData ( c )); }
            relocating = this.relocateCentroids( clusterMeans );
        }

        return this.centroids;
    }

    @Override
    public String executeWithTestData(Object testData) {
        return null;
    }
}

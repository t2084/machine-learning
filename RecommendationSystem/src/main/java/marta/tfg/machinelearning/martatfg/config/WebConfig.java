package marta.tfg.machinelearning.martatfg.config;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@ComponentScan("marta.tfg.machinelearning.martatfg")
public class WebConfig implements WebMvcConfigurer
{
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedHeaders("Accept", "Content-Type", "Origin",
                        "Authorization", "X-Auth-Token", "Access-Control-Allow-Origin")
                .exposedHeaders("X-Auth-Token", "Authorization", "Access-Control-Allow-Origin")
                .allowedMethods("POST", "GET", "DELETE", "PUT", "OPTIONS");
    }

}

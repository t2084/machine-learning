package marta.tfg.machinelearning.martatfg.statistics;

import java.util.ArrayList;

/**
 *
 * @author martafernandezgarcia
 *
 */
public class Statistics {
    /**
     *
     * @param elements list of double items
     * @return the variance
     */
    public static double variance( ArrayList<Double> elements ) {
        double xSum = 0;
        double elementsAvg = Statistics.average(elements);
        for(Double element: elements) {
            xSum = (element - elementsAvg);
        }
        return xSum/elements.size();
    }

    /**
     *
     * @param x list of double elements
     * @param y list of double elements
     * @return the covariance
     * @throws Exception if lists do not have the same size
     */
    public static double covariance( ArrayList<Double> x, ArrayList<Double> y ) throws Exception {
        if (x.size() != y.size()) throw new Exception("Both lists must have the same size");

        double xSum, ySum, globalSum = 0;
        double xAvg = Statistics.average(x), yAvg = Statistics.average(y);

        for(int i = 0; i<x.size(); i++){
            xSum = (x.get(i)-xAvg);
            ySum = (y.get(i)-yAvg);

            globalSum += xSum*ySum;
        }

        return globalSum/(x.size()*1.0);
    }

    /**
     *
     * @param x list of double elements
     * @param y list of double elements
     * @return the pearsonCoefficient
     * @throws Exception if lists do not have the same size
     */
    public static double pearsonCoefficient( ArrayList<Double> x, ArrayList<Double> y ) throws Exception {
        if (x.size() != y.size()) throw new Exception("Both lists must have the same size");

        double xSum = .0, ySum = .0, xPowSum = .0, yPowSum = .0;
        double xyProd = .0;

        for(int i=0; i<x.size(); i++) {
            xyProd += x.get(i)*y.get(i);
            xSum += x.get(i);
            ySum += y.get(i);
            xPowSum += x.get(i)*x.get(i);
            yPowSum += y.get(i)*y.get(i);
        }

        return (x.size()*xyProd-xSum*ySum)/(Math.sqrt(
                ((x.size()*xPowSum)-(xSum*xSum))*((x.size()*yPowSum)-(ySum*ySum))
        ));
    }

    /**
     *
     * @return the average of a list of double elements
     */
    public static double average( ArrayList<Double> elements ) {
        double sum = 0;
        for( Double element: elements ) {
            sum += element;
        }
        return sum/elements.size();
    }

}

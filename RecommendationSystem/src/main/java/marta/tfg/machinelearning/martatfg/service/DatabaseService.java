package marta.tfg.machinelearning.martatfg.service;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.model.Objective;
import marta.tfg.machinelearning.martatfg.model.Rating;
import marta.tfg.machinelearning.martatfg.model.User;
import marta.tfg.machinelearning.martatfg.repository.DatabaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Marta Fernández García
 */
@Service
public class DatabaseService implements IDatabaseService{
    private static final Logger LOG = LoggerFactory.getLogger(DatabaseService.class);
    public static final String PERSISTENCE_UNIT_NAME = "database";
    private final EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    private EntityManager em = null;

    @Autowired
    DatabaseRepository repository;

    /**
     * Method which initializes the database
     */
    public void initialize() {
        this.repository.db_tables = new ConcurrentHashMap<>();
        this.getJSONConfig();

        this.repository.attributesImportance = new TreeMap<>();
        this.setTableAttributesImportance(DatabaseRepository.CLEANUSERS);
        this.setTableAttributesImportance(DatabaseRepository.CLEANOBJECTIVE);
        this.setDatabaseTables();
        this.fillDatabaseTables();
    }

    /**
     * Initializes the map with the names of the tables and empty lists
     */
    private void setDatabaseTables() {
        this.repository.db_tables.put(DatabaseRepository.CLEANUSERS, new ArrayList<>());
        this.repository.db_tables.put(DatabaseRepository.CLEANOBJECTIVE, new ArrayList<>());
        this.repository.db_tables.put(DatabaseRepository.RATINGS, new ArrayList<>());
    }

    /**
     * It fills all the tables with the database values
     */
    private void fillDatabaseTables() {
        this.repository.db_tables.forEach( ( key, value ) -> {
            this.em = factory.createEntityManager();
            Query query = this.em.createNativeQuery("SELECT * FROM " + key, Tuple.class);
            List<Tuple> registers = query.getResultList();
            List<String> columns = new ArrayList<>();

            for ( Tuple register : registers ) {
                List<TupleElement<?>> elements = register.getElements();
                for (TupleElement<?> element : elements ) {
                    columns.add( element.getAlias() );
                }
            }
            if ( key.equalsIgnoreCase(DatabaseRepository.CLEANUSERS) ) this.fillUserDatabaseTable( columns, registers );
            if ( key.equalsIgnoreCase(DatabaseRepository.CLEANOBJECTIVE) ) this.fillObjectiveDatabaseTable( columns, registers );
            if ( key.equalsIgnoreCase(DatabaseRepository.RATINGS) ) this.fillRatingDatabaseTable ( columns, registers );
            this.em.close();
        });
    }

    /**
     *
     * @param columns name
     * @param registers the records in the table
     */
    private void fillRatingDatabaseTable ( List<String> columns, List<Tuple> registers ) {
        Object value;
        Rating rating;

        for ( Tuple register : registers ) {
            rating = new Rating();
            for ( String column : columns ) {
                if ( column.equalsIgnoreCase("created_at") || column.equalsIgnoreCase("updated_at")) continue;
                value = register.get(column);
                if ( column.equalsIgnoreCase("songId") ) rating.setSongId( value.toString() );
                if ( column.equalsIgnoreCase("userId") ) rating.setUserId( value.toString() );
                if ( column.equalsIgnoreCase("liked") )  rating.setLiked( Boolean.parseBoolean(value.toString()) );
            }
            this.repository.db_tables.get( DatabaseRepository.RATINGS ).add( rating );
        }
        LOG.info("Database " + DatabaseRepository.RATINGS + " initialized correctly");
    }

    /**
     *
     * @param columns name
     * @param registers the records in the table
     */
    private void fillObjectiveDatabaseTable( List<String> columns, List<Tuple> registers ) {
        Objective objective;
        String value;

        for ( Tuple register : registers ) {
            objective = new Objective();
            objective.setAttributesImportance( this.repository.attributesImportance.get( DatabaseRepository.CLEANOBJECTIVE ));
            for ( String column : columns ) {
                value = register.get(column).toString();
                if ( column.equalsIgnoreCase("row_names") ) continue;
                if ( column.equalsIgnoreCase(DatabaseRepository.OBJECTIVEPK) ) objective.setId( value );
                objective.addAttribute( column, value );
            }
            this.repository.db_tables.get( DatabaseRepository.CLEANOBJECTIVE ).add( objective );
        }
        LOG.info("Database " + DatabaseRepository.CLEANOBJECTIVE + " initialized correctly");
    }

    /**
     *
     * @param columns name
     * @param registers the records in the table
     */
    private void fillUserDatabaseTable( List<String> columns, List<Tuple> registers ) {
        User user;
        String value;
        boolean sexSet;

        for ( Tuple register : registers ) {
            sexSet = false;
            user = new User();
            user.setAttributesImportance( this.repository.attributesImportance.get( DatabaseRepository.CLEANUSERS ));
            for (String column : columns) {
                value = register.get(column).toString();
                if ( column.contains( DatabaseRepository.USERSPK ) ) user.setUser_id(value);
                else if ( column.contains("sex") && !sexSet ) {
                    if ( value.equals("1") ) user.setUser_sex(User.Sex.FEMALE);
                    else user.setUser_sex(User.Sex.MALE);
                    sexSet = true;
                } else if ( column.contains("age") ) {
                    user.setUser_age( Integer.parseInt(value) );
                } else if ( column.contains("country") ) {
                    user.setUser_country( value );
                }
            }
            this.repository.db_tables.get( DatabaseRepository.CLEANUSERS ).add( user );
        }
        LOG.info("Database " + DatabaseRepository.CLEANUSERS + " initialized correctly");
    }

    public Map<String, ArrayList<Model>> getDatabaseTables() {
        return this.repository.db_tables;
    }

    @Override
    public String getCleanUsers() {
        return DatabaseRepository.CLEANUSERS;
    }

    @Override
    public String getCleanObjective() {
        return DatabaseRepository.CLEANOBJECTIVE;
    }

    /**
     * It reads the JSON config file and establish the tables name and their primary keys
     */
    public void getJSONConfig() {
        File myFile = new File("../core/conf.json");
        String line;
        String[] entry;

        try {
            Scanner sc = new Scanner(myFile);

            while (sc.hasNextLine()) {
                line = sc.nextLine();
                if (line.contains("objectiveTable")) {
                    entry = line.split(":");
                    DatabaseRepository.CLEANOBJECTIVE = PREFIX
                            + entry[1].substring(2, entry[1].length() - 2);
                }
                if (line.contains("userTable")) {
                    entry = line.split(":");
                    DatabaseRepository.CLEANUSERS = PREFIX + entry[1].substring(2, entry[1].length() - 2);
                }
                if (line.contains("userPK")) {
                    entry = line.split(":");
                    DatabaseRepository.USERSPK = entry[1].substring(2, entry[1].length() - 2);
                }
                if (line.contains("objectivePK")) {
                    entry = line.split(":");
                    DatabaseRepository.OBJECTIVEPK = entry[1].substring(2, entry[1].length() - 2);
                }

                if (line.contains("ratings")) {
                    entry = line.split(":");
                    DatabaseRepository.RATINGS = entry[1].substring(2, entry[1].length() - 2);
                }
            }
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param tableName it gets the database table name importance related table and fill the attributesImportance table
     */
    public Map<String, Integer> setTableAttributesImportance( String tableName ) {
        this.em = factory.createEntityManager();
        Query query = this.em.createNativeQuery("SELECT * FROM " + tableName + "Importance" , Tuple.class );
        List<Tuple> registers =  query.getResultList();
        Map<String, Integer> attributeImportance = new TreeMap<>();

        for ( Tuple register : registers ) {
            List<TupleElement<?>> registerElements = register.getElements();
            for ( TupleElement<?> p : registerElements ) {
                if ( !register.get( p ).toString().equals("0") ) {
                    attributeImportance.put( p.getAlias(), Integer.parseInt(register.get(p).toString()) );
                }
            }
        }
        this.em.close();
        this.repository.attributesImportance.put(tableName, attributeImportance);
        return this.repository.attributesImportance.get(tableName);
    }

    /**
     *
     * @param userId user id to find
     * @return the user
     */
    public Model findUserById( String userId ) {
        ArrayList<Model> users = this.getDatabaseTables().get(DatabaseRepository.CLEANUSERS);
        for ( Model user : users ) {
            if ( user.getId().equals( userId ) ) {
                return user;
            }
        }
        return null;
    }

    /**
     *
     * @param objectiveId objective id to find
     * @return the objective
     */
    public Model findObjectiveById ( String objectiveId ) {
        ArrayList<Model> objectives = this.getDatabaseTables().get( DatabaseRepository.CLEANOBJECTIVE );
        for ( Model objective : objectives ) {
            if ( objective.getId().equals( objectiveId ) ) return objective;
        }
        return null;
    }

    /**
     *
     * @param userId the user id we want the relationship from
     * @return all objective items the user liked
     */
    public Set<String> getAllLikedObjectivesByUser( String userId ) {
        ArrayList<Model> ratings = this.getDatabaseTables().get( DatabaseRepository.RATINGS );
        Set<String> objectivesIds = new HashSet<>();
        for ( Model m : ratings ) {
            if ( !m.getAttributes().get("userId").equals(userId) ) continue;
            if (m.getAttributes().get("liked").equals("1")) {
                objectivesIds.add( m.getAttributes().get(DatabaseRepository.OBJECTIVEPK) );
            }
        }
        return objectivesIds;
    }

    /**
     *
     * @param userId user id
     * @param field to order by
     * @return
     */
    public ArrayList<String> getAllLikedObjectivesByUserOrderByField( String userId, String field ) {
        this.em = factory.createEntityManager();
        String queryString = "SELECT * FROM " + DatabaseRepository.RATINGS + " WHERE userId = " + userId
                + " ORDER BY " + field + " ASC";
        Query query = this.em.createNativeQuery( queryString
                , Tuple.class );
        List<Tuple> registers =  query.getResultList();
        ArrayList<String> records = new ArrayList<>();

        for ( Tuple register : registers ) {
            List<TupleElement<?>> registerElements = register.getElements();
            for ( TupleElement<?> p : registerElements ) {
                if ( p.getAlias().equalsIgnoreCase( DatabaseRepository.OBJECTIVEPK ) ) {
                    records.add( register.get(p).toString() );
                }
            }
        }

        this.em.close();
        return records;
    }

    /**
     *
     * @param tableName database table name
     * @param field field to select
     * @param whereField field in the where condition
     * @param whereValue value for the given field
     * @return the query result
     */
    public String getValueForField( String tableName, String field, String whereField, String whereValue ) {
        this.em = factory.createEntityManager();
        String queryString = "SELECT " + field + " FROM " + tableName + " WHERE " + whereField + " = '" + whereValue + "'" ;
        Query query = this.em.createNativeQuery( queryString
                , Tuple.class );

        List<Tuple> registers =  query.getResultList();
        ArrayList<String> records = new ArrayList<>();

        for ( Tuple register : registers ) {
            List<TupleElement<?>> registerElements = register.getElements();
            for ( TupleElement<?> p : registerElements ) {
                records.add( register.get(p).toString() );
            }
        }

        this.em.close();
        return records.toString();
    }

    /**
     *
     * @param objectiveId the objective we want to find the features from
     * @return the features (feature + value)
     */
    public Multimap<String, String> getObjectiveFeatures(String objectiveId) {
        Multimap<String,String> objectiveFeatures = TreeMultimap.create();
        TreeMap<String, String> objectiveAttributes = this.findObjectiveById( objectiveId ).getAttributes();
        for ( Map.Entry<String, String> attribute : objectiveAttributes.entrySet() ) {
            objectiveFeatures.put(attribute.getKey(), attribute.getValue());
        }
        return objectiveFeatures;
    }
}

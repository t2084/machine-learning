package marta.tfg.machinelearning.martatfg.service;

import com.google.common.collect.Multimap;
import marta.tfg.machinelearning.martatfg.model.Model;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/**
 * @author Marta Fernández García
 */
@Service
public interface IDatabaseService {
    String PREFIX = "ml_clean";
    void initialize();
    String getCleanUsers();
    String getCleanObjective();
    void getJSONConfig();
    Map<String, ArrayList<Model>>  getDatabaseTables();
    Map<String, Integer> setTableAttributesImportance( String tableName );
    Model findUserById(String userId);
    Model findObjectiveById(String objectiveId);
    Set<String> getAllLikedObjectivesByUser(String userId);
    Multimap<String, String> getObjectiveFeatures(String objectiveId);
}

package marta.tfg.machinelearning.martatfg.mlkit.algorithms.decisiontree;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Marta Fernandez Garcia
 */
public class DecisionNode<T> extends Node<T>{
    private String question;
    private Map<String, Node<T>> paths;

    public DecisionNode() {
        this.question = "";
        this.paths = new TreeMap<>();
    }

    /**
     *
     * @param question
     * @param paths
     */
    public DecisionNode(String question, Map<String, Node<T>> paths) {
        this.question = question;
        this.paths = paths;
    }

    /**
     *
     * @param question
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     *
     * @param paths
     */
    public void setPaths(Map<String, Node<T>> paths) {
        this.paths = paths;
    }

}

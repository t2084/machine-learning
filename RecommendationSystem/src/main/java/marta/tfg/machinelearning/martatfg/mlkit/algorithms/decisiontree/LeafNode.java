package marta.tfg.machinelearning.martatfg.mlkit.algorithms.decisiontree;

/**
 * @author Marta Fernandez Garcia
 */
public class LeafNode<T> extends Node<T>{
    T value;

    public LeafNode(){
        this.value = null;
    }

    /**
     *
     * @param value
     */
    public LeafNode( T value ) {
        this.value = value;
    }

    /**
     *
     * @param value
     */
    public void setValue(T value) {
        this.value = value;
    }
}

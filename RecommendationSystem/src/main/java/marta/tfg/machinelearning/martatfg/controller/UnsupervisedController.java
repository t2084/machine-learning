package marta.tfg.machinelearning.martatfg.controller;

import marta.tfg.machinelearning.martatfg.executors.UnsupervisedExecution;
import marta.tfg.machinelearning.martatfg.mlkit.filtering.CollaborativeFiltering;
import marta.tfg.machinelearning.martatfg.mlkit.filtering.ContentBasedFiltering;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author Marta Fernández García
 */
@RestController
@RequestMapping("/unsupervised")
public class UnsupervisedController {
    @Autowired
    UnsupervisedExecution uEx;
    @Autowired
    ContentBasedFiltering cbf;
    @Autowired
    CollaborativeFiltering cf;

    /**
     *
     * @param tableName database table name
     * @param modelId database table record id
     * @return the cluster it belongs to with the other data in the cluster
     */
    @RequestMapping(path="kmeans", method = RequestMethod.GET)
    public Set<?> kmeans( @RequestParam String tableName, @RequestParam String modelId ) {
        return this.uEx.kmeans( tableName, modelId );
    }

    /**
     *
     * @param userId user database id
     * @param numItems num of items we want to return to the user
     * @return tree map with the property and the num of matches
     */
    @RequestMapping(path="contentBasedFiltering", method = RequestMethod.GET)
    public TreeMap<String, Integer> contentBasedFiltering(@RequestParam String userId, @RequestParam int numItems) {
        return this.cbf.cbfFiltering( userId, numItems );
    }

    /**
     *
     * @param userId user to recommend the objectives
     * @param numItems num of items to return to the response
     * @return the objectives to recommend to the user
     */
    @RequestMapping(path="collaborativeFiltering", method = RequestMethod.GET)
    public String collaborativeFilteringUserUser(@RequestParam String userId, @RequestParam int numItems) {
        return this.cf.collaborativeFiltering(userId, numItems).toString();
    }
}

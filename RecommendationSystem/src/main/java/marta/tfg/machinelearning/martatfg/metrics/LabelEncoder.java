package marta.tfg.machinelearning.martatfg.metrics;

import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author Marta Fernández García
 */
@Component
public class LabelEncoder {

    @Autowired
    DatabaseService databaseService;
    private TreeMap<String, TreeMap<String, Double>> encodedMapping;
    double count = 0;

    public void initialize() {
        this.encodedMapping = new TreeMap<>();
    }

    /**
     *
     * @param tableName database table name
     * @param property the property we want to encode
     */
    public void labelEncoder( String tableName, String property ) {
        TreeMap<String, Double> auxEncodings = new TreeMap<>();
        ArrayList<Model> tableRecords = this.databaseService.getDatabaseTables().get(tableName);
        Set<String> values = this.setPropertyValues( tableRecords, property );

        for ( String value : values ) {
            auxEncodings.put(value, count);
            count ++;
        }

        this.encodedMapping.put(property, auxEncodings);
    }

    /**
     *
     * @param tableRecords database table records
     * @param property the property we want to encode
     * @return set with the database table property
     */
    private Set<String> setPropertyValues ( ArrayList<Model> tableRecords, String property ) {
        Set<String> values = new HashSet<>();
        for ( Model m : tableRecords ) { values.add( m.getAttributes().get( property ) ); }
        return values;
    }

    /**
     *
     * @param property property to find the encoded value
     * @param field property field
     * @return encoded value
     */
    public Double getValueByProperty( String property, String field ) {
        return this.encodedMapping.get( property ).get( field );
    }

}

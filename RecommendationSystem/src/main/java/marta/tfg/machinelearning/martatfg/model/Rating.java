package marta.tfg.machinelearning.martatfg.model;

import marta.tfg.machinelearning.martatfg.repository.DatabaseRepository;

import java.util.TreeMap;

public class Rating extends Model{
    private boolean liked;
    private String objectiveId;
    private String userId;

    public Rating() {
        this.liked = false;
        this.objectiveId = "";
        this.userId = "";
    }

    public String getSongId() {
        return objectiveId;
    }

    public void setSongId( String objectiveId ) {
        this.objectiveId = objectiveId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId( String userId ) {
        this.userId = userId;
    }

    public boolean getLiked() {
        return this.liked;
    }

    public void setLiked( boolean liked ) {
        this.liked = liked;
    }

    @Override
    public TreeMap<String, String> getAttributes() {
        TreeMap<String, String> attributes = new TreeMap<>();
        attributes.put("liked", this.liked ? "1" : "0");
        attributes.put(DatabaseRepository.OBJECTIVEPK, this.objectiveId);
        attributes.put("userId", this.userId);
        return attributes;
    }

    @Override
    public int getAttributeImportance(String attribute) {
        return 0;
    }

    @Override
    public String toString() {
        return "{" +
                "liked=" + liked +
                ", objectiveId='" + objectiveId + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }

    /**
     * Not of interest
     * @param o the other rating
     * @return 0
     */
    @Override
    public int compareTo(Model o) {
        return 0;
    }
}

package marta.tfg.machinelearning.martatfg.mlkit;

import marta.tfg.machinelearning.martatfg.mlkit.algorithms.decisiontree.DecisionTree;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.kmeans.Cluster;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.kmeans.KMeans;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.knn.KNearestNeighbors;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.knn.Neighbour;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.linearregression.LinearRegression;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.naivebayes.NaiveBayes;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.model.Objective;
import marta.tfg.machinelearning.martatfg.model.User;
import marta.tfg.machinelearning.martatfg.repository.DatabaseRepository;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import marta.tfg.machinelearning.martatfg.service.IDatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author Marta Fernández García
 */
@Component
public class MLKit {

	@Autowired
	IDatabaseService service;
	@Autowired
	KMeans kmeans;
	@Autowired
	DecisionTree<Boolean> decisionTree;
	@Autowired
	NaiveBayes naiveBayes;
	private static final Logger LOG = LoggerFactory.getLogger(DatabaseService.class);

	/**
	 * @return linear regression algorithm result
	 * @throws Exception if not numeric field or not liner distribution
	 */
	public String linearRegression(ArrayList<Model> table, String[] fields) throws Exception {
		TreeMap<String, ArrayList<Double>> storedValues = new TreeMap<>();

		LOG.info("Linear Regression 1");
		/* Check if fields are numeric */
		for ( String field : fields ) {
			if ( table.get(0).getAttributes().containsKey(field) ) {
				try {
					Double.parseDouble(table.get(0).getAttributes().get(field));
				} catch( Exception e ) {
					throw new RuntimeException(field + " field is not numeric.");
				}
			}
		}

		ArrayList<Double> values;
		List<String> fieldsA = Arrays.asList(fields);

		for ( Model m : table ) {
			for ( Entry<String, String> attributes : m.getAttributes().entrySet() ) {
				if ( !fieldsA.contains(attributes.getKey()) ) continue;
				if ( storedValues.containsKey(attributes.getKey()) ) {
					values = storedValues.get(attributes.getKey());
					values.add(Double.parseDouble(attributes.getValue()));
				} else {
					values = new ArrayList<>();
					values.add(Double.parseDouble(attributes.getValue()));
				}
				storedValues.put(attributes.getKey(), values);
			}
		}

		LOG.info("Linear Regression 2");


		double[] result = new double[2];
		LinearRegression lr = new LinearRegression(storedValues.get(fields[0]), storedValues.get(fields[1]));

		lr.linearRegression();
		result[0] = lr.getA();
		result[1] = lr.getB();
		return "y=" + result[0] + "x + " + result[1];
	}

	/**
	 *
	 * @param initialData all database table information
	 * @param givenInformation the known information
	 * @param testPointProperties properties we want to test (prediction)
	 * @return naive bayes algorithm result
	 */
	public String naiveBayes( ArrayList<Model> initialData, TreeMap<String,String> givenInformation,
			TreeMap<String,String> testPointProperties ) {
		return Double.toString(this.naiveBayes.naiveBayes(initialData, givenInformation, testPointProperties)*100);
	}

	/**
	 *
	 * @param tableName database table name
	 * @param table database table data
	 * @param properties information we know about the record
	 * @param missingProperty the property we want to figure out
	 * @return kNeighbors algorithm result
	 */
	public String kNeighbors(String tableName, ArrayList<Model> table, TreeMap<String, String> properties, String missingProperty) {
		/* Data transformation, from arraylist to hashmap*/
		HashMap<Neighbour, Double> trainingPoints = new HashMap<>();
		KNearestNeighbors kn = new KNearestNeighbors();
		Neighbour n;
		Neighbour testPoint = null;

		/* Preparing trainingPoints. Each neighbour with the value of the missing property */
		for (Model m : table) {
			if (properties.containsKey("id") && properties.get("id").equals(m.getId())) continue;
			/* Set class label */
			if (m.getAttributes().get(missingProperty).equals("sex")) {
				if (m.getAttributes().get("sex").equals("1")) {
					n = new Neighbour(m, User.Sex.FEMALE.toString());
				} else {
					n = new Neighbour(m, User.Sex.MALE.toString());
				}
			} else {
				n = new Neighbour(m, m.getAttributes().get(missingProperty));
			}
			trainingPoints.put(n, .0);
		}

		/* TestPoint */
		if (tableName.equalsIgnoreCase(service.getCleanUsers())) {
			User user = new User();
			if (!missingProperty.equals("id")) user.setUser_id(properties.get("id"));
			if (!missingProperty.equals("country")) user.setUser_country(properties.get("country"));
			if (!missingProperty.equals("age")) user.setUser_age(Integer.parseInt(properties.get("age")));
			if (!missingProperty.equals("sex")) {
				if (properties.get("sex").equals("male")) user.setUser_sex(User.Sex.MALE);
				if (properties.get("sex").equals("female")) user.setUser_sex(User.Sex.FEMALE);
			}
			user.attributesImportance = service.setTableAttributesImportance(DatabaseRepository.CLEANUSERS);
			testPoint = new Neighbour(user);

		} else if (tableName.equalsIgnoreCase(service.getCleanObjective())) {
			Objective obj = new Objective();
			obj.setAttributes(properties);
			obj.attributesImportance = service.setTableAttributesImportance(DatabaseRepository.CLEANOBJECTIVE);
			testPoint = new Neighbour(obj);
		}

		return kn.kNearesNeighbors(trainingPoints, testPoint, "prediction");
	}

	/**
	 *
	 * @param tableName database table name
	 * @param table database table data
	 * @param properties all the record properties
	 * @return kNeighbors algorithm result
	 */
	public String kNeighborsSupervised( String tableName, ArrayList<Model> table, TreeMap<String, String> properties ) {
		HashMap<Neighbour, Double> trainingPoints = new HashMap<>();
		KNearestNeighbors kn = new KNearestNeighbors();
		Neighbour n;
		Neighbour testPoint = null;
		String idField = (tableName.equals(DatabaseRepository.CLEANUSERS)) ? DatabaseRepository.USERSPK : DatabaseRepository.OBJECTIVEPK;

		/* Preparing trainingPoints */
		for (Model m : table) {
			if (properties.containsKey(idField) && properties.get(idField).equals(m.getId())) continue;
			n = new Neighbour(m, m.getId());
			trainingPoints.put(n, .0);
		}

		/* TestPoint */
		if (tableName.equalsIgnoreCase(DatabaseRepository.CLEANUSERS)) {
			Model user = service.findUserById( properties.get( DatabaseRepository.USERSPK) );
			user.attributesImportance = service.setTableAttributesImportance(DatabaseRepository.CLEANUSERS);
			testPoint = new Neighbour(user);
		} else if (tableName.equalsIgnoreCase(DatabaseRepository.CLEANOBJECTIVE)) {
			Model obj = service.findObjectiveById( properties.get(DatabaseRepository.OBJECTIVEPK) );
			testPoint = new Neighbour(obj);
		}

		return kn.kNearesNeighbors(trainingPoints, testPoint, "supervised");
	}


	/**
	 *
	 * @param tableName the database table name we are working with
	 * @param data the database table data
	 * @param m the model we want to check which cluster belongs to
	 * @return the set of points where the model m belongs to
	 */
	public Set<?> kMeans( String tableName, ArrayList<Model> data, Model m ) {
		kmeans.initialize( tableName, data );
		Set<Cluster> kMeansResult = kmeans.kmeans();
		return m == null ? kMeansResult :  kmeans.findClusterByItem(m).getData();
	}

	public String decisionTree ( String userId, String objectiveId ) {
		decisionTree.initialize( objectiveId, this.service.getAllLikedObjectivesByUser(userId) );
		return decisionTree.decisionTree();
	}
}

package marta.tfg.machinelearning.martatfg.executors;

import marta.tfg.machinelearning.martatfg.mlkit.MLKit;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * 
 * @author martafernandezgarcia
 *
 */
@Component
public class PredictionExecution{

	private static final Logger LOG = LoggerFactory.getLogger(DatabaseService.class);
	@Autowired
	DatabaseService databaseService;
	@Autowired
	MLKit mlKit;

	/**
	 *
	 * @param table database table name
	 * @param fields database table records
	 * @return linear regression algorithm result
	 * @throws Exception if not numeric field or not linear distribution
	 */
	public String prepareLinearRegression ( String table, String[] fields) throws Exception {
		Map<String, ArrayList<Model>> databaseTables = databaseService.getDatabaseTables();
		String loweredCasedTable = DatabaseService.PREFIX + table.toLowerCase();
		int count = 0;

		for ( String field : fields ) {
			if ( field.contains("sex") ) fields[count] = "sex";
			count ++;
		}

		if ( databaseTables.containsKey(loweredCasedTable) ) {
			LOG.info("PRUEBA 1");
			return mlKit.linearRegression(databaseTables.get(loweredCasedTable), fields);
		} else {
			LOG.info(databaseTables.keySet() + " doesn't contain: " + DatabaseService.PREFIX + loweredCasedTable);
			return "The database doesn't contain the table which name is: " + DatabaseService.PREFIX + loweredCasedTable;
		}
	}

	/**
	 *
	 * @param table database table name
	 * @param properties properties we know with their value
	 * @param missingProperty property column name we don't know
	 * @return knn algorithm result
	 */
	public String prepareKNN( String table, TreeMap<String, String> properties,
			String missingProperty ) {
		Map<String, ArrayList<Model>> databaseTables = databaseService.getDatabaseTables();
		String loweredCasedTable = DatabaseService.PREFIX + table.toLowerCase();

		if ( databaseTables.containsKey(loweredCasedTable) ) {
			return mlKit.kNeighbors(loweredCasedTable, databaseTables.get(loweredCasedTable), properties, missingProperty);
		} else {
			LOG.info(databaseTables.keySet() + " doesn't contain: " + DatabaseService.PREFIX + loweredCasedTable);
			return "The database doesn't contain the table which name is: " + DatabaseService.PREFIX + loweredCasedTable;
		}
	}

	/**
	 *
	 * @param table database table name
	 * @param givenInformation properties we know their value
	 * @param predictValuesMap properties we want to predict
	 * @return naive bayes algorithm result
	 */
	public String prepareNaiveBayes( String table, TreeMap<String, String> givenInformation,
			TreeMap<String, String> predictValuesMap ) {
		Map<String, ArrayList<Model>> databaseTables = databaseService.getDatabaseTables();
		String loweredCasedTable = DatabaseService.PREFIX + table.toLowerCase();

		if ( databaseTables.containsKey( loweredCasedTable ) ) {
			return mlKit.naiveBayes(databaseTables.get(loweredCasedTable), givenInformation , predictValuesMap);
		} else {
			LOG.info(databaseTables.keySet() + " doesn't contain: " + DatabaseService.PREFIX + loweredCasedTable);
			return "The database doesn't contain the table which name is: " + DatabaseService.PREFIX + loweredCasedTable;
		}
	}

	/**
	 *
	 * @param tableName database table name
	 * @return kMeans algorithm result
	 */
	public String prepareKMeans( String tableName ) {
		String databaseTableName = DatabaseService.PREFIX + tableName.toLowerCase();
		return mlKit.kMeans( databaseTableName,databaseService.getDatabaseTables().get( databaseTableName ), null ).toString();
	}

	/**
	 *
	 * @param userId if the user will like the given objective
	 * @param objectiveId the objective to guess if the user wil like
	 * @return yes/no question
	 */
	public String prepareDecisionTree( String userId, String objectiveId ) {
		return mlKit.decisionTree( userId, objectiveId );
	}
}

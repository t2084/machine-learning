package marta.tfg.machinelearning.martatfg.mlkit.algorithms.knn;

import marta.tfg.machinelearning.martatfg.metrics.Metrics;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.model.Objective;
import marta.tfg.machinelearning.martatfg.model.User;
import marta.tfg.machinelearning.martatfg.repository.DatabaseRepository;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * @author martafernandezgarcia
 */
public class Neighbour implements Comparable<Neighbour> {
    private Model data;
    private Double distance;
    private String classLabel;

    /**
     * @param data
     */
    public Neighbour(Model data) {
        this.data = data;
        this.distance = .0;
        this.classLabel = "?";
    }

    /**
     * @param data
     * @param classLabel
     */
    public Neighbour(Model data, String classLabel) {
        this.data = data;
        this.distance = .0;
        this.classLabel = classLabel;
    }

    /**
     * @param other
     * @return
     */
    public double calculateDistance(Neighbour other) {
        double totalDistance = .0;
        /* Type + data */
        HashMap<String, ArrayList<Object>> dataType = new HashMap<>();
        ArrayList<Object> aux;

        /* Check which data is numerical and which one is categorical */
        for (Entry<String, String> attributes : this.data.getAttributes().entrySet()) {
            try {
                if (!dataType.containsKey("numerical")) aux = new ArrayList<>();
                else aux = dataType.get("numerical");
                Double.parseDouble(attributes.getValue());
                aux.add(attributes.getKey());
                dataType.put("numerical", aux);
            } catch (Exception e) {
                if (!dataType.containsKey("categorical")) aux = new ArrayList<>();
                else aux = dataType.get("categorical");
                aux.add(attributes.getKey());
                dataType.put("categorical", aux);
            }
        }

        /* Calculate the distance */
        for (Entry<String, String> attributes : this.data.getAttributes().entrySet()) {
            if ( this.data instanceof Objective && attributes.getKey().equals(DatabaseRepository.OBJECTIVEPK)) continue;
            else if ( this.data instanceof User && attributes.getKey().equals(DatabaseRepository.USERSPK)) continue;

            String value;
            if ((value = this.findValue(dataType, attributes.getKey())) == null) continue;
            if (attributes.getValue() == null) continue;
            int importance = 1;
            if ( this.data.attributesImportance.containsKey(attributes.getKey() ) ) {
                importance = this.data.getAttributeImportance( attributes.getKey() );
            }
            if (value.equals("numerical")) {
                double thisData = Double.parseDouble(attributes.getValue());
                double otherData = Double.parseDouble(other.getData().getAttributes().get(attributes.getKey()));
                totalDistance += ((Math.log(importance))*Math.abs(thisData - otherData))%10;
            } else {
                totalDistance += (importance)*Metrics.levenshteinDistance(
                        attributes.getValue(), other.getData().getAttributes().get(attributes.getKey()));
            }
        }

        return totalDistance;
    }

    /**
     * @param list
     * @param value
     * @return
     */
    private String findValue(HashMap<String, ArrayList<Object>> list, Object value) {
        for (Entry<String, ArrayList<Object>> elements : list.entrySet()) {
            if (elements.getValue().contains(value)) {
                return elements.getKey();
            }
        }
        return null;
    }

    @Override
    public int compareTo(Neighbour o) {
        return this.distance.compareTo(o.getDistance());
    }

    /**
     * @return
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * @param distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     * @return
     */
    public String getClassLabel() {
        return classLabel;
    }

    /**
     * @param classLabel
     */
    public void setClassLabel(String classLabel) {
        this.classLabel = classLabel;
    }

    /**
     * @param data
     */
    public void setData(Model data) {
        this.data = data;
    }

    /**
     * @return
     */
    public Model getData() {
        return this.data;
    }

    @Override
    public String toString() {
        return this.data.toString() + " == " + this.distance;
    }

}

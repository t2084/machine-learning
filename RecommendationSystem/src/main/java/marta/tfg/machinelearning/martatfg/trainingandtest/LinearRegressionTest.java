package marta.tfg.machinelearning.martatfg.trainingandtest;

import marta.tfg.machinelearning.martatfg.mlkit.algorithms.IAlgorithm;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.linearregression.LinearRegression;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Marta Fernández García
 */
@Component
public class LinearRegressionTest implements ITrainTest{

    @Override
    public IAlgorithm train() {
        ArrayList<Double> x = new ArrayList<>(Arrays.asList(1.0,2.0,3.0,4.0,5.0,6.0));
        ArrayList<Double> y = new ArrayList<>(Arrays.asList(2.0,4.0,6.0,8.0,10.0,12.0));
        LinearRegression lr;
        try {
            lr = new LinearRegression(x,y);
            lr.linearRegression();
        }  catch (Exception e) { throw new RuntimeException(e.getMessage()); }
        return lr;
    }

    @Override
    public String test() {
        LinearRegression lrTrained = (LinearRegression)this.train();
        ArrayList<Double> xValues = new ArrayList<>(Arrays.asList(7.0,8.0,20.0, 50.0));
        ArrayList<Integer> expectedResults = new ArrayList<>(Arrays.asList(14,16,40, 100));
        long result;
        int sumCorrect = 0, sumIncorrect = 0, count = 0;

        for ( Double x : xValues ) {
            result = Math.round(Double.parseDouble(lrTrained.executeWithTestData( x )));
            if ( result == (expectedResults.get(count))  ) {
                sumCorrect ++;
            } else {
                sumIncorrect ++;
            }
            count ++;
        }

        return "Model trained: y = "  + lrTrained.getA() + "x + " + lrTrained.getB() + "\n" +
                "Correct instances = " + sumCorrect + "/" + count + " \nIncorrect instances = " + sumIncorrect + "/" + count;
    }

}

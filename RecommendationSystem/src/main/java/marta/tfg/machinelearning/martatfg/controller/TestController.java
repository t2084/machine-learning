package marta.tfg.machinelearning.martatfg.controller;

import marta.tfg.machinelearning.martatfg.mlkit.algorithms.knn.Neighbour;
import marta.tfg.machinelearning.martatfg.model.User;
import marta.tfg.machinelearning.martatfg.trainingandtest.KnnTrainTest;
import marta.tfg.machinelearning.martatfg.trainingandtest.LinearRegressionTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.github.javafaker.Faker;
import java.io.FileWriter;
import java.util.*;

/**
 * @author Marta Fernández García
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    KnnTrainTest knTest;

    @Autowired
    LinearRegressionTest lrTest;
    Faker faker = new Faker();

    /**
     *
     * @return random user data with a class
     */
    private ArrayList<Neighbour> generateRandomUserWithClass() {
        String row;
        ArrayList<Neighbour> result = new ArrayList<>();
        FileWriter myWriter;
        User.Sex[] sex = {User.Sex.FEMALE, User.Sex.MALE};
        String randomSex, randomName, randomAddress, randomClass;
        User user;
        Neighbour n;
        String[] classes = {"yes", "no"};
        try {
            myWriter = new FileWriter("/var/www/html/machine-learning/cleaningData/initial_data.txt");

            row = "userId\tname\tage\tsex\tcountry\tlabel\n";
            myWriter.write(row);
            for ( int i=1; i<=500; i++ ) {
                randomSex = sex[(int)Math.round(Math.random())].toString();
                randomName = faker.name().firstName();
                randomAddress = faker.address().streetAddress();
                randomClass = classes[(int)Math.round(Math.random())];
                user = new User();
                user.setUser_id(Integer.toString(i));
                user.setUser_age(i);
                user.setUser_sex( User.Sex.valueOf(randomSex) );
                user.setUser_country( randomAddress );
                user.setUser_age(i*10);
                n = new Neighbour(user,randomClass);
                result.add(n);
                row = i+"\t"+randomName+"\t"+(i*10)+"\t"+randomSex+"\t"+randomAddress+"\t"+randomClass+"\n";
                myWriter.write(row);
            }

            myWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }


    @RequestMapping(path="linearregression", method = RequestMethod.GET)
    public String testLinearRegression() {
        this.lrTest.train();
        return this.lrTest.test();
    }
}

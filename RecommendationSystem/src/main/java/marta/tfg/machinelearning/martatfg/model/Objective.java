package marta.tfg.machinelearning.martatfg.model;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author martafernandezgarcia
 *
 */
public class Objective extends Model {

	/*
	 * As we don't know which are the attributes, they will be saved in a treemap
	 * attribute_name => value
	 */
	private TreeMap<String, String> attributes;

	public Objective() {
		this.attributes = new TreeMap<>();
		this.attributesImportance = new TreeMap<>();
	}

	public void addAttribute( String attribute, String value ) {
		this.attributes.put( attribute, value );
	}
	public void setAttributes( TreeMap<String, String> newAttributes ) {
		this.attributes = newAttributes;
	}
	@Override
	public TreeMap<String, String> getAttributes() {
		return this.attributes;
	}

	@Override
	public String toString() {
		return this.attributes.toString();
	}


	/**
	 *
	 * @param attributesImportance
	 */
	public void setAttributesImportance( Map<String, Integer> attributesImportance ) {
		this.attributesImportance = attributesImportance;
	}

	/**
	 *
	 * @param attribute
	 * @return
	 */
	public int getAttributeImportance( String attribute ) {
		return this.attributesImportance.get( attribute );
	}

	/**
	 * Cannot implement this method because we don't know the attributes
	 * @param o the other model
	 * @return null
	 */
	@Override
	public int compareTo(Model o) {
		return 0;
	}
}

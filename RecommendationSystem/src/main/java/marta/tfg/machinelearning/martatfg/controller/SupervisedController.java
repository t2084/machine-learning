package marta.tfg.machinelearning.martatfg.controller;

import marta.tfg.machinelearning.martatfg.executors.SupervisedExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.TreeMap;

/**
 * @author Marta Fernández García
 */
@RestController
@RequestMapping("/supervised")
public class SupervisedController {
    @Autowired
    SupervisedExecution sEx;

    /**
     *
     * @param tableName database table name
     * @param knownProperties properties we know the value of
     * @return knn algorithm result
     */
    @RequestMapping(path="knn", method = RequestMethod.GET)
    public String knn(@RequestParam String tableName, @RequestParam String[] knownProperties ) {
        TreeMap<String, String> knownPropertiesMap = new TreeMap<>();
        String[] propertyValue;
        for ( String property : knownProperties ) {
            propertyValue = property.split("=");
            knownPropertiesMap.put(propertyValue[0], propertyValue[1]);
        }
        return this.sEx.prepareKNN( tableName, knownPropertiesMap );
    }

    /**
     *
     * @param tableName database table name
     * @param userId the user id
     * @param objectiveId the objective id
     * @param field the database table field
     * @return percentage
     */
    @RequestMapping(path="naiveBayes", method = RequestMethod.GET)
    public String naiveBayes(@RequestParam String tableName, @RequestParam String userId,
                             @RequestParam String objectiveId, @RequestParam String field ) {
        return this.sEx.prepareNaiveBayes( tableName, userId, objectiveId, field );
    }



}

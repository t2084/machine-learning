package marta.tfg.machinelearning.martatfg.trainingandtest;

import marta.tfg.machinelearning.martatfg.mlkit.algorithms.IAlgorithm;

/**
 * @author Marta Fernández García
 */
public interface ITrainTest {
    IAlgorithm train();
    String test();
}

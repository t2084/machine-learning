package marta.tfg.machinelearning.martatfg.repository;

import marta.tfg.machinelearning.martatfg.model.Model;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Map;

@Repository
public class DatabaseRepository {

    public Map<String, ArrayList<Model>> db_tables;
    public Map<String, Map<String, Integer>> attributesImportance;
    public static String CLEANUSERS = "";
    public static String CLEANOBJECTIVE = "";
    public static String USERSPK = "";
    public static String OBJECTIVEPK = "";
    public static String RATINGS = "";
}

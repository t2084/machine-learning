package marta.tfg.machinelearning.martatfg.mlkit.algorithms.decisiontree;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.IAlgorithm;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import marta.tfg.machinelearning.martatfg.statistics.Operations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * @author Marta Fernandez Garcia
 */
@Component
public class DecisionTree<T> implements IAlgorithm {

    @Autowired
    DatabaseService databaseService;
    private Table<Integer, String, String> ratingInformation = HashBasedTable.create();
    DecisionNode<T> root = null;

    /**
     *
     * @param objectiveId the objective we want to check it the user will like
     * @param objectivesLiked all the liked objectives by the user
     */
    public void initialize( String objectiveId, Set<String> objectivesLiked ) {
        int internalId = 1;
        Model objective;

        // Objective values for the row
        for ( String objectivesRated : objectivesLiked) {
            objective = this.databaseService.findObjectiveById( objectivesRated );
            for ( String objectiveAttribute : objective.getAttributes().keySet() ) {
                if ( objectiveAttribute.contains("id") ) continue;
                ratingInformation.put(internalId, objectiveAttribute, objective.getAttributes().get( objectiveAttribute ) );
            }
            ratingInformation.put(internalId, "liked", "1");
            internalId ++;
        }
    }

    /**
     *
     * @param data dataset where to find the possible values
     * @param property property to find the possible values
     * @return the possible values from the property given
     */
    private Set<String> getPropertyPossibleValues ( Table<Integer, String, String> data, String property ) {
        Set<String> result = new HashSet<>();
        Set<Table.Cell<Integer, String, String>> dataSet = data.cellSet();
        for (Table.Cell<Integer, String, String> values : dataSet ) {
            if ( values.getColumnKey().equals(property) ) result.add( values.getValue() );
        }
        return result;
    }

    /**
     *
     * @param data dataset where to find the column with its value
     * @param column the column to calculate the entropy to
     * @param value the value to calculate the entropy to
     * @return entropy
     */
    private double calculateEntropy ( Table<Integer, String, String> data, String column, String value ) {
        Map<String, Map<Integer, String>> dataSet = data.columnMap();
        Map<Integer, String> columnData = dataSet.get(column);
        Map<Integer, String> ratingData = dataSet.get("liked");
        int totalInstancesForColumn = 0, totalLiked = 0, totalUnLiked = 0;
        double likedRatio, unlikedRatio, logLiked, logUnliked;

        for (Map.Entry<Integer, String> cData : columnData.entrySet() ) {
            if ( cData.getValue().equals ( value ) ) {
                totalInstancesForColumn ++;
                if ( ratingData.get( cData.getKey() ).equals("true") ) { totalLiked++; }
                else { totalUnLiked++; }
            }
        }

        likedRatio = ((double)totalLiked/totalInstancesForColumn);
        unlikedRatio = ((double)totalUnLiked/totalInstancesForColumn);

        logLiked = (-likedRatio)*(-Operations.LOG2(likedRatio));
        logUnliked = (-unlikedRatio)*(-Operations.LOG2(unlikedRatio));

        logLiked = Double.isNaN(logLiked) ? .0 : logLiked;
        logUnliked = Double.isNaN(logUnliked) ? .0 : logUnliked;

        return logLiked+logUnliked;
    }


    /**
     *
     * @param data the dataset where to find the number of instances of the property
     * @param column the property
     * @return the number of instances of a property
     */
    public int getPropertyNumOfInstances( Table<Integer, String, String> data, String column ) {
        Map<String, Map<Integer, String>> dataSet = data.columnMap();
        Map<Integer, String> columnData = dataSet.get(column);

        return columnData.size();
    }

    /**
     *
     * @param data where to find the number of instances of the property with a given value
     * @param column the property
     * @param value the value
     * @return the number of instances of a property with a given value
     */
    public int getPropertyValueNumOfInstances( Table<Integer, String, String> data, String column, String value ) {
        Map<String, Map<Integer, String>> dataSet = data.columnMap();
        Map<Integer, String> columnData = dataSet.get(column);
        int totalInstancesForColumn = 0;
        for (Map.Entry<Integer, String> cData : columnData.entrySet() ) {
            if ( cData.getValue().equals ( value ) ) {
                totalInstancesForColumn ++;
            }
        }

        return totalInstancesForColumn;
    }


    public String decisionTree()
    {
        Set<String> initialProperties = this.ratingInformation.columnKeySet(); // [duration, genre]
        Set<String> possibleValues;
        TreeMap<String, Double> propertyEntropy = new TreeMap<>();
        TreeMap<String, Double> valueEntropy = new TreeMap<>();
        double totalEntropy;

        int nTotalPropertyInstances, nTotalPropertyValueInstances;

        /*
            En teoria el proceso va a ser el mismo para el resto de iteraciones, lo unico que debe cambiar es que en initialProperties
            debemos ir sacando las que se vayan eligiendo y que, en funcion del camino se debe tener una tabla u otra (this.ratingInformation)
         */
        // Calculate the initial entropy for all the properties
        for ( String property : initialProperties ) {
            totalEntropy = .0;
            nTotalPropertyInstances = this.getPropertyNumOfInstances( this.ratingInformation, property );
            possibleValues = this.getPropertyPossibleValues ( this.ratingInformation, property );

            // Calculate the entropy for all the column values
            for ( String value : possibleValues ) {
                valueEntropy.put(value,this.calculateEntropy( this.ratingInformation, property, value ));
            }

            // Calculate the total entropy of the column
            for ( Map.Entry<String, Double> vEntropy : valueEntropy.entrySet() ) {
                nTotalPropertyValueInstances = this.getPropertyValueNumOfInstances( this.ratingInformation, property, vEntropy.getKey() );
                totalEntropy += ((double)nTotalPropertyValueInstances/nTotalPropertyInstances)*vEntropy.getValue();
            }

            propertyEntropy.put(property, totalEntropy);
        }

        return this.ratingInformation.columnKeySet().toString();
    }
    @Override
    public String executeWithTestData(Object testData) {
        return null;
    }

}

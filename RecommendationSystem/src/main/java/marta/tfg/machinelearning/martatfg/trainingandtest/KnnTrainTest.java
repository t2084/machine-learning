package marta.tfg.machinelearning.martatfg.trainingandtest;

import marta.tfg.machinelearning.martatfg.mlkit.algorithms.IAlgorithm;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.knn.KNearestNeighbors;
import marta.tfg.machinelearning.martatfg.mlkit.algorithms.knn.Neighbour;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * @author Marta Fernández García
 */
@Component
public class KnnTrainTest implements ITrainTest{

    Neighbour testPoint;
    HashMap<Neighbour, Double> dataMap;
    @Autowired
    KNearestNeighbors knn;

    /**
     *
     * @param data initial data
     * @param testPoint test point
     */
    public String initialize(ArrayList<Neighbour> data, Neighbour testPoint, int k ) {
        KNearestNeighbors.K = k;
        Collections.shuffle(data);
        this.testPoint = testPoint;
        this.dataMap = new HashMap<>();

        for ( Neighbour n : data ) { this.dataMap.put( n, .0 ); }

        return this.test();
    }

    @Override
    public IAlgorithm train() {
        return null;
    }

    @Override
    public String test() {
        return this.knn.kNearesNeighbors(this.dataMap, testPoint, "supervised" );
    }
    
}
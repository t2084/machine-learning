package marta.tfg.machinelearning.martatfg.metrics;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author martafernandezgarcia
 *
 */
public class Metrics {

	/**
	 *
	 * @param x point value
	 * @param y point value
	 * @return absolute difference
	 */
	public static double absDifference(double x, double y) {
		return Math.abs(x-y);
	}

	/**
	 *
	 * @param x point value
	 * @param y point value
	 * @return euclidean distance
	 */
	public static double eclideanDistance(List<Double> x, List<Double> y) {
		double diff = .0;		
		if ( x.size() != y.size()) throw new RuntimeException("Both lists must have the same size");
		for(int i=0; i<x.size(); i++){
			diff += (x.get(i) - y.get(i));
		}
		return Math.abs(diff);
	}

	/**
	 * @param x string value
	 * @param y string value
	 * @return levenshtein distance
	 */
	public static int levenshteinDistance(String x, String y) {
	    int[][] dp = new int[x.length() + 1][y.length() + 1];

	    for (int i = 0; i <= x.length(); i++) {
	        for (int j = 0; j <= y.length(); j++) {
	            if (i == 0) {
	                dp[i][j] = j;
	            }
	            else if (j == 0) {
	                dp[i][j] = i;
	            }
	            else {
	                dp[i][j] = min(dp[i - 1][j - 1] 
	                 + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)), 
	                  dp[i - 1][j] + 1, 
	                  dp[i][j - 1] + 1);
	            }
	        }
	    }

	    return dp[x.length()][y.length()];
	}

	/**
	 *
	 * @param a character
	 * @param b character
	 * @return if their are equals or not. Not equals implies a change
	 */
    private static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }

	/**
	 *
	 * @param numbers array of numbers
	 * @return min of all of them
	 */
    private static int min(int... numbers) {
        return Arrays.stream(numbers)
          .min().orElse(Integer.MAX_VALUE);
    }
}

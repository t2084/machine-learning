package marta.tfg.machinelearning.martatfg.mlkit.algorithms.kmeans;

import marta.tfg.machinelearning.martatfg.model.Model;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Marta Fernández García
 */
public class Cluster implements Comparable<Cluster> {
    private double center;
    private final Set<Model> data;

    /**
     *
     * @param center the coordinates center
     */
    public Cluster(double center) {
        this.center = center;
        this.data = new HashSet<>();
    }

    /**
     *
     * @param m the model to add to the cluster
     */
    public void addData ( Model m ) {
        this.data.add ( m );
    }

    /**
     *
     * @return all the data in the cluster
     */
    public Set<Model> getData () {
        return this.data;
    }

    /**
     *
     * @return the cluster center
     */
    public double getCenter() {
        return this.center;
    }

    /**
     *
     * @param center new center
     */
    public void setCenter( double center ) { this.center = center; }

    @Override
    public String toString() {
        return "Center={" + this.center + "}, data=" + this.data;
    }

    @Override
    public int compareTo(Cluster o) {
        return Double.compare(this.center, o.center);
    }
}

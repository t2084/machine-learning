package marta.tfg.machinelearning.martatfg.model;

import javax.persistence.Id;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author martafernandezgarcia
 * Database models must extends this class
 *
 */
public abstract class Model implements Comparable<Model> {
	@Id
	protected String id;
	public Map<String, Integer> attributesImportance;
	public String getId() { return id; }
	public void setId( String id ) { this.id = id; }
	public abstract TreeMap<String, String> getAttributes();
	public abstract int getAttributeImportance( String attribute );
}


package marta.tfg.machinelearning.martatfg.executors;

import marta.tfg.machinelearning.martatfg.mlkit.MLKit;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.repository.DatabaseRepository;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UnsupervisedExecution {
    @Autowired
    DatabaseService databaseService;
    @Autowired
    MLKit mlKit;

    /**
     *
     * @param tableName
     * @param modelId
     * @return
     */
    public Set<?> kmeans( String tableName, String modelId ) {
        String loweredCasedTable = DatabaseService.PREFIX + tableName.toLowerCase();
        Model m = null;
        if ( loweredCasedTable.equals( DatabaseRepository.CLEANOBJECTIVE ) ) m = this.databaseService.findObjectiveById( modelId );
        else if ( loweredCasedTable.equals( DatabaseRepository.CLEANUSERS ) ) m = this.databaseService.findUserById ( modelId );
        return mlKit.kMeans( loweredCasedTable, databaseService.getDatabaseTables().get(loweredCasedTable), m);
    }


}

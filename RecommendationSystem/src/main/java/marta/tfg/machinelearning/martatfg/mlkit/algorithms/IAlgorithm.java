package marta.tfg.machinelearning.martatfg.mlkit.algorithms;

/**
 * @author Marta Fernández García
 */
public interface IAlgorithm {
    String executeWithTestData(Object testData);
}

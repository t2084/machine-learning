package marta.tfg.machinelearning.martatfg.mlkit.algorithms.naivebayes;

import marta.tfg.machinelearning.martatfg.mlkit.algorithms.IAlgorithm;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.service.IDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * 
 * @author martafernandezgarcia
 *
 */
@Component
public class NaiveBayes implements IAlgorithm {
	private ArrayList<Model> initialData;
	private TreeMap<String, String> information;
	private TreeMap<String, String> conditions;

	/**
	 * Returns the probability condition happens given information
	 * @return
	 */
	private Double calculateConditionedProbabilities() {		
		int count = 0;
		int totalCount = 0;
		double result;

		/*
		 * It may seem not efficient but information and conditions are formed by one entry.
		 * Complexity order is given by initialData size
		 */
		for ( Entry<String, String> fieldValueInfo : information.entrySet() ) {
			count = 0;
			totalCount = 0;
			for ( Entry<String, String> fieldValueCondition : conditions.entrySet() ) {
				for ( Model m : initialData ) {
					if ( m.getAttributes().get(fieldValueCondition.getKey()).equals(fieldValueCondition.getValue())  ) {
						totalCount ++;
							if ( m.getAttributes().get(fieldValueInfo.getKey()).equals(fieldValueInfo.getValue()) ) {
								count ++;								
							}
					}
				}
			}
		}
				
		result = (1.0*count/totalCount);
		return Double.isNaN(result) ? 0.0 : result;
	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	private Double calculateProbability(TreeMap<String, String> data) {
		double count = 0, totalCount = 0;
		for ( Entry<String, String> attributes : data.entrySet() ) {
			for ( Model m : initialData ) { 
				if ( m.getAttributes().get(attributes.getKey()).equals(attributes.getValue()) )  count ++;
				totalCount ++;
			}
		}
		
		return count/totalCount;
	}
	
	/**
	 * 
	 * @return
	 */
	public double naiveBayes(ArrayList<Model> initialData, TreeMap<String, String> information,
							 TreeMap<String, String> conditions) {
		this.initialData = initialData;
		this.information = information;
		this.conditions = conditions;
		double prob = this.calculateConditionedProbabilities();
		double conditionProb = this.calculateProbability(this.conditions);
		double infoProb = this.calculateProbability(this.information);
		double result = (prob*conditionProb)/(infoProb);
		
		return Double.isNaN(result) ? 0.0 : result;
	}

	@Override
	public String executeWithTestData(Object testData) {
		return null;
	}
}

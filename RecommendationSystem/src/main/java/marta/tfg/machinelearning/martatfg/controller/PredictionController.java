package marta.tfg.machinelearning.martatfg.controller;

import marta.tfg.machinelearning.martatfg.executors.PredictionExecution;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.TreeMap;

/**
 * @author Marta Fernández García
 */
@RestController
@RequestMapping("/prediction")
public class PredictionController {
    @Autowired
    PredictionExecution pEx;
    private static final Logger LOG = LoggerFactory.getLogger(DatabaseService.class);


    /**
     *
     * @param tableName database table name we are going to work with
     * @param fields database table fields
     * @return linear regression algorithm result
     */
    @RequestMapping(path="linearRegression", method = RequestMethod.GET)
    public String linearRegression( @RequestParam String tableName, @RequestParam String[] fields ) {
        try {
            LOG.info("EMPEZANDO A PREPARAR LINEAR REGRESSION");
            return this.pEx.prepareLinearRegression( tableName, fields );
        } catch (Exception exception) {
            LOG.info(exception.getMessage());
            LOG.info(exception.toString());

            return exception.getMessage();
        }
    }

    /**
     *
     * @param tableName database table name we are going to work with
     * @param knownProperties the properties we know about the record
     * @param missingProperty the properties we want to predict about the record
     * @return knn algorithm result
     */
    @RequestMapping(path="knn", method = RequestMethod.GET)
    public String knn(@RequestParam String tableName, @RequestParam String[] knownProperties,
                    @RequestParam String missingProperty ) {
        TreeMap<String, String> knownPropertiesMap = new TreeMap<>();
        String[] propertyValue;
        for ( String property : knownProperties ) {
            propertyValue = property.split("=");
            knownPropertiesMap.put(propertyValue[0], propertyValue[1]);
        }
        return this.pEx.prepareKNN( tableName, knownPropertiesMap, missingProperty );
    }

    /**
     *
     * @param tableName database table name we are going to work with
     * @param givenInformation the properties we know about the record
     * @param predictValues the properties we want to predict about the record
     * @return naive bayes algorithm result
     */
    @RequestMapping(path="naiveBayes", method = RequestMethod.GET)
    public String naiveBayes(@RequestParam String tableName, @RequestParam String[] givenInformation,
                              @RequestParam String[] predictValues ) {

        TreeMap<String, String> givenInformationMap = new TreeMap<>();
        TreeMap<String, String> predictValuesMap = new TreeMap<>();
        String[] split;
        for ( String information : givenInformation ) {
            split = information.split("=");
            givenInformationMap.put(split[0], split[1]);
        }

        for ( String information : predictValues ) {
            split = information.split("=");
            predictValuesMap.put(split[0], split[1]);
        }
        return this.pEx.prepareNaiveBayes( tableName, givenInformationMap, predictValuesMap );
    }

    /**
     *
     * @param tableName the database table name we are working with
     * @return kMeans algorithm result
     */
    @RequestMapping(path="kmeans", method = RequestMethod.GET)
    public String kmeans(@RequestParam String tableName ) {
        return this.pEx.prepareKMeans( tableName );
    }

    @RequestMapping(path="decisionTree", method = RequestMethod.GET)
    public String decisionTree(@RequestParam String userId, @RequestParam String objectiveId ) { return this.pEx.prepareDecisionTree( userId, objectiveId ); }

}
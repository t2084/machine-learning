package marta.tfg.machinelearning.martatfg.executors;

import marta.tfg.machinelearning.martatfg.mlkit.MLKit;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.repository.DatabaseRepository;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Marta Fernández García
 *
 */
@Component
public class SupervisedExecution {
    private static final Logger LOG = LoggerFactory.getLogger(DatabaseService.class);
    @Autowired
    DatabaseService databaseService;
    @Autowired
    MLKit mlKit;

    /**
     *
     * @param table database table name
     * @param properties properties with their value
     * @return knn algorithm result
     */
    public String prepareKNN( String table, TreeMap<String, String> properties ) {
        Map<String, ArrayList<Model>> databaseTables = databaseService.getDatabaseTables();
        String loweredCasedTable = DatabaseService.PREFIX + table.toLowerCase();

        if ( databaseTables.containsKey(loweredCasedTable) ) {
            return mlKit.kNeighborsSupervised( loweredCasedTable, databaseTables.get(loweredCasedTable), properties );
        } else {
            LOG.info(databaseTables.keySet() + " doesn't contain: " + DatabaseService.PREFIX + loweredCasedTable);
            return "The database doesn't contain the table which name is: " + DatabaseService.PREFIX + loweredCasedTable;
        }
    }

    /**
     *
     * @param tableName database table name
     * @param userId user to recommend to
     * @param objectiveId song which we want to calculate the probability of like to
     * @return the probability of liking the song
     */
    public String prepareNaiveBayes( String tableName, String userId, String objectiveId, String field ) {
        String loweredCasedTable = DatabaseService.PREFIX + tableName.toLowerCase();
        ArrayList<Model> tableData = databaseService.getDatabaseTables().get( loweredCasedTable );
        String lastRatedObjective = databaseService.getAllLikedObjectivesByUserOrderByField( userId, "created_at" ).get(0);
        String tableId = tableName.equals(DatabaseRepository.CLEANUSERS) ? DatabaseRepository.USERSPK : DatabaseRepository.OBJECTIVEPK;
        TreeMap<String, String> testPointProperties = new TreeMap<>();
        TreeMap<String, String> givenInformation = new TreeMap<>();

        String lastRatedObjectiveField = databaseService.getValueForField( loweredCasedTable, field, tableId, lastRatedObjective );
        String objectiveField = databaseService.getValueForField( loweredCasedTable, field, tableId, objectiveId );

        testPointProperties.put( tableId, lastRatedObjectiveField );
        givenInformation.put( tableId, objectiveField );

        return mlKit.naiveBayes( tableData, givenInformation, testPointProperties );
    }
}

package marta.tfg.machinelearning.martatfg.model;

import marta.tfg.machinelearning.martatfg.metrics.Metrics;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author martafernandezgarcia
 *
 */
public class User extends Model {

	/* These attributes are required for the system recommendation. Any other attribute is not considered to be relevant*/
	public enum Sex {
		MALE, FEMALE
	}
	private Sex user_sex;
	private int user_age;
	private String user_country;
	
	public User() {
		this.attributesImportance = new TreeMap<>();
	}
	
	public String getUser_id() {
		return this.id;
	}
	public void setUser_id( String user_id ) {
		this.id = user_id;
	}
	public Sex getUser_sex() {
		return user_sex;
	}
	public void setUser_sex( Sex user_sex ) {
		this.user_sex = user_sex;
	}
	public int getUser_age() {
		return user_age;
	}
	public void setUser_age( int user_age ) {
		this.user_age = user_age;
	}
	public String getUser_country() {
		return user_country;
	}
	public void setUser_country( String user_country ) {
		this.user_country = user_country;
	}

	@Override
	public TreeMap<String, String> getAttributes() {
		TreeMap<String, String> attributes = new TreeMap<>();
		attributes.put("userId", this.getUser_id());
		attributes.put("age", Integer.toString(this.getUser_age()));
		attributes.put("sex", ( this.getUser_sex() == Sex.FEMALE) ? "1" : (this.getUser_sex() == Sex.MALE) ? "0" : null );
		attributes.put("country", this.getUser_country());
		return attributes;
	}

	@Override
	public String toString() {
		return "{id=" + this.getUser_id() + ", sex=" + this.getUser_sex() + ", age=" + this.getUser_age() + 
				", country=" + this.getUser_country() +  "}";
	}

	/**
	 *
	 * @param attributesImportance set the attribute importance
	 */
	public void setAttributesImportance( Map<String, Integer> attributesImportance ) {
		this.attributesImportance = attributesImportance;
	}

	/**
	 *
	 * @param attribute to get the importance of
	 * @return the importance
	 */
	public int getAttributeImportance( String attribute ) {
		return this.attributesImportance.get( attribute );
	}

	@Override
	public int compareTo(Model o) {
		return (int) Metrics.absDifference(this.getUser_age(), ((User) o).getUser_age())
				+ Metrics.levenshteinDistance(this.getUser_country(), ((User) o).getUser_country());
	}
}

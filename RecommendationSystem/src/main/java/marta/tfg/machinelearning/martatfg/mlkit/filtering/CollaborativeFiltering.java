package marta.tfg.machinelearning.martatfg.mlkit.filtering;

import marta.tfg.machinelearning.martatfg.metrics.Metrics;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.repository.DatabaseRepository;
import marta.tfg.machinelearning.martatfg.service.IDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * @author Marta Fernández García
 */
@Component
public class CollaborativeFiltering {

    @Autowired
    IDatabaseService databaseService;
    TreeMap<String, TreeMap<String, Boolean>> ratingMatrix; // user -> objective --> liked
    TreeMap<String, TreeMap<String, Double>> weighingMatrix; // user -> objective --> weighing
    TreeMap<String, Double> evaluationMatrix; // objective -> evaluation
    TreeMap<String, Double> similarity;
    String userId;

    /**
     *
     * @param userId the user to recommend a objective to
     * @param numItems to send to the response
     * @return an objective to recommend
     */
    public Set<String> collaborativeFiltering(String userId, int numItems ) {
        HashSet<String> result = new HashSet<>();

        this.userId = userId;
        this.ratingMatrix = this.fillRatingMatrix();
        this.similarity = this.setSimilarityBetweenUsers();
        this.weighingMatrix = this.fillWeighingMatrix();
        this.evaluationMatrix = this.fillEvaluationMatrix();

        int nItemsToShow = Math.min(numItems, this.evaluationMatrix.entrySet().size());
        int count = 0;

        for ( Map.Entry<String, Double> entryEvaluation : this.evaluationMatrix.entrySet() ) {
            if ( count >= nItemsToShow ) break;
            result.add( entryEvaluation.getKey() );
            count ++;
        }
        return result;
    }


    /**
     *
     * @return evaluation matrix
     */
    private TreeMap<String, Double> fillEvaluationMatrix() {
        TreeMap<String, Double> evaluationMatrix = new TreeMap<>();

        for ( Map.Entry<String, TreeMap<String, Double>> entryMatrix : this.weighingMatrix.entrySet() ) {
            for ( Map.Entry<String, Double> entryValue : entryMatrix.getValue().entrySet() ) {
                evaluationMatrix.put(entryValue.getKey(), this.sumOfWeightsOfUsersWhoRatedTheObjective( entryValue.getKey() ));
            }
        }

        return evaluationMatrix;
    }

    /**
     *
     * @return weighing matrix
     */
    private TreeMap<String, TreeMap<String, Double>> fillWeighingMatrix() {
        TreeMap<String, TreeMap<String, Double>> weighingMatrix = new TreeMap<>();
        TreeMap<String, Double> aux;
        double weighing;

        for ( Map.Entry<String, TreeMap<String, Boolean>> entryUser : this.ratingMatrix.entrySet() ) {
            aux = new TreeMap<>();
            if (entryUser.getKey().equals(this.userId)) continue;
            for ( Map.Entry<String, Boolean> entryRating : entryUser.getValue().entrySet() ) {
                if ( this.userHasRatedObjective( entryRating.getKey() ) ) continue;
                weighing = entryRating.getValue() ? 1.0*this.similarity.get(entryUser.getKey()) : 0.0;
                if ( !weighingMatrix.containsKey(entryUser.getKey()) ) {
                    aux.put( entryRating.getKey(), weighing );
                } else {
                    aux = weighingMatrix.get(entryUser.getKey());
                    if ( !aux.containsKey(entryRating.getKey()) ) {
                        aux.put( entryRating.getKey(), weighing );
                    }
                }
                weighingMatrix.put( entryUser.getKey(), aux );
            }
        }

        return weighingMatrix;
    }

    /**
     *
     * @param objectiveId the objective id
     * @return the sum of the weights of all the users who rated the given objective
     */
    private double sumOfWeightsOfUsersWhoRatedTheObjective( String objectiveId ) {
        double sumOfWeights = .0, sumOfRatings = .0;
        for ( Map.Entry<String, TreeMap<String, Boolean>> entryUser : this.ratingMatrix.entrySet() ) {
            if ( entryUser.getKey().equals ( this.userId ) ) continue;
            for ( Map.Entry<String, Boolean> entryRating : entryUser.getValue().entrySet() ) {
                if ( entryRating.getKey().equals( objectiveId ) ) {
                    sumOfWeights += this.similarity.get( entryUser.getKey() );
                    sumOfRatings += entryRating.getValue() ? 1 : 0;
                    break;
                }
            }
        }

        return sumOfRatings/sumOfWeights;
    }

    /**
     *
     * @param objectiveId the objective to check if user rated
     * @return if user rated the objective
     */
    private boolean userHasRatedObjective ( String objectiveId ) {
        return this.ratingMatrix.get(userId).containsKey(objectiveId);
    }

    /**
     *
     * @return treemap with user and similarity
     */
    private TreeMap<String, Double> setSimilarityBetweenUsers () {
        TreeMap<String, Double> distanceUsers = new TreeMap<>();
        TreeMap<String, Double> similarityUsers = new TreeMap<>();
        Model userAux, user;
        double distance;
        user = databaseService.findUserById( this.userId );
        for ( Map.Entry<String, TreeMap<String, Boolean>> userEntry : this.ratingMatrix.entrySet() ){
            if ( userEntry.getKey().equals( this.userId ) ) continue;
            userAux = databaseService.findUserById( userEntry.getKey() );
            distance = calculateDistance( user, userAux );
            distanceUsers.put( userEntry.getKey(), distance );
        }

        Double maxDistance = .0;

        for ( Map.Entry<String, Double> distanceUser : distanceUsers.entrySet() ) {
            if ( distanceUser.getValue() > maxDistance ) {
                maxDistance = distanceUser.getValue();
            }
        }

        for ( Map.Entry<String, Double> distanceUser : distanceUsers.entrySet() ) {
            similarityUsers.put(distanceUser.getKey(), 1-(distanceUser.getValue()/maxDistance));
        }

        return similarityUsers;
    }

    /**
     *
     * @param m1 one of the users
     * @param m2 the other user
     * @return similarity
     */
    private double calculateDistance( Model m1, Model m2 ) {
        String attribute;
        double distance = .0;
        int totalNumProperties = m1.getAttributes().entrySet().size()-1;

        for ( Map.Entry<String, String> attributes : m1.getAttributes().entrySet() ) {
            if ( m1.getId().equals(m2.getId() )) continue;
            attribute = attributes.getKey();
            if ( attribute.equals(DatabaseRepository.USERSPK)) continue;

            if ( this.isNumeric( m1.getAttributes().get(attribute) ) && isNumeric(m2.getAttributes().get( attribute )) ) {
                distance += Metrics.absDifference(Double.parseDouble(m1.getAttributes().get(attribute)), Double.parseDouble(m2.getAttributes().get(attribute)) );
            } else {
                distance += Metrics.levenshteinDistance( m1.getAttributes().get(attribute), m2.getAttributes().get(attribute) );
            }
        }

        return (distance/totalNumProperties);
    }

    /**
     *
     * @param value to check if is numeric
     * @return if is numeric
     */
    private boolean isNumeric( String value ) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @return the rating matrix
     */
    private TreeMap<String, TreeMap<String, Boolean>> fillRatingMatrix() {
        TreeMap<String, TreeMap<String, Boolean>> ratingMatrix = new TreeMap<>();
        TreeMap<String, Boolean> objectiveLiked;
        ArrayList<Model> ratings = this.databaseService.getDatabaseTables().get(DatabaseRepository.RATINGS);
        String userId,objectiveId;
        boolean liked;

        for (Model m : ratings) {
            objectiveLiked = new TreeMap<>();
            userId = m.getAttributes().get(DatabaseRepository.USERSPK);
            objectiveId = m.getAttributes().get(DatabaseRepository.OBJECTIVEPK);
            liked = !Boolean.parseBoolean(m.getAttributes().get("liked"));

            if (!ratingMatrix.containsKey(userId)) {
                objectiveLiked.put(objectiveId, liked);
            } else {
                objectiveLiked = ratingMatrix.get(userId);

                if (!objectiveLiked.containsKey(objectiveId)) {
                    objectiveLiked.put(objectiveId, liked);
                }
            }
            ratingMatrix.put(userId, objectiveLiked);
        }

        return ratingMatrix;
    }

}

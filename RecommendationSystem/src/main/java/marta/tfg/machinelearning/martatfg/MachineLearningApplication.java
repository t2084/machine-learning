package marta.tfg.machinelearning.martatfg;
import marta.tfg.machinelearning.martatfg.service.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;

@SpringBootApplication()
@ComponentScan("marta.tfg.machinelearning.martatfg")
public class MachineLearningApplication {
	private static final Logger LOG = LoggerFactory.getLogger(MachineLearningApplication.class);

	@Autowired
	DatabaseService databaseService;
	public static void main(String[] args) {

		SpringApplication.run( MachineLearningApplication.class, args );
	}

	@EventListener(ApplicationReadyEvent.class)
	public void initializeDatabase() {
		LOG.info("Initializing database ... ");
		databaseService.initialize();
	}
}


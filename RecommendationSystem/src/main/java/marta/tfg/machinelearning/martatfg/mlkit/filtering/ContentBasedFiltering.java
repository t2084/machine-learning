package marta.tfg.machinelearning.martatfg.mlkit.filtering;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import marta.tfg.machinelearning.martatfg.model.Model;
import marta.tfg.machinelearning.martatfg.repository.DatabaseRepository;
import marta.tfg.machinelearning.martatfg.service.IDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * @author Marta Fernandez Garcia
 */
@Component
public class ContentBasedFiltering {
    /**
     *
     * @param map map to sort
     * @param <K> key
     * @param <V> value
     * @return map sorted by value
     */
    public static <K, V extends Comparable<V> > TreeMap<K, V>  valueSort(final TreeMap<K, V> map)
    {
        Comparator<K> valueComparator = (k1, k2) -> {
            int comp = map.get(k1).compareTo(map.get(k2));
            if (comp == 0)  return 1;
            else  return -comp;
        };

        TreeMap<K, V> sorted = new TreeMap<>(valueComparator);
        sorted.putAll(map);
        return sorted;
    }

    @Autowired
    IDatabaseService databaseService;
    private Multimap<String, String> desiredFeatures;
    private TreeMap<String, Integer> objectiveNumOfMatched;
    private Set<String> userLikedObjectivesId;

    /**
     *
     * @param userId user who liked the item
     * @param numItems to return to the map
     * @return map with numItems elements
     */
    public TreeMap<String, Integer> cbfFiltering( String userId, int numItems ) {
        this.desiredFeatures = TreeMultimap.create();
        this.objectiveNumOfMatched = new TreeMap<>();

        this.userLikedObjectivesId = databaseService.getAllLikedObjectivesByUser(userId);
        this.calculateDesiredFeatures();
        this.objectiveNumOfMatched = this.fillFeatureMatrix( numItems );

        return this.objectiveNumOfMatched;
    }

    private void calculateDesiredFeatures() {
        for ( String objectiveId  : this.userLikedObjectivesId ) {
            desiredFeatures.putAll(databaseService.getObjectiveFeatures( objectiveId ));
        }
    }

    /**
     *
     * @return Return for each record of the objective table the number of matches between the features of the object
     */
    private TreeMap<String, Integer> fillFeatureMatrix( int numItems ) {
        ArrayList<Model> objectiveData = databaseService.getDatabaseTables().get(DatabaseRepository.CLEANOBJECTIVE);
        TreeMap<String, Integer> auxiliarMatched = new TreeMap<>();
        int matched = 0, count = 0;

        for ( Model m : objectiveData ) {
            for ( Map.Entry<String, Collection<String>> entryFeature : this.desiredFeatures.asMap().entrySet() ) {
                if ( this.desiredFeatures.get(DatabaseRepository.OBJECTIVEPK).contains(m.getId()) ) continue;
                if ( entryFeature.getValue().contains( m.getAttributes().get(entryFeature.getKey()) ) ) {
                    matched ++;
                }
            }
            auxiliarMatched.put ( m.getId(), matched );
            matched = 0;
        }

        auxiliarMatched = valueSort(auxiliarMatched);
        for ( Map.Entry<String, Integer> songMatches : auxiliarMatched.entrySet() ) {
            if ( count >= numItems ) break;
            this.objectiveNumOfMatched.put( songMatches.getKey(), songMatches.getValue() );
            count ++;
        }

        return objectiveNumOfMatched;
    }

}

# Step by step 

The first step to start working with this project is to execute
the core/ini.sh. This field will read the conf.json content. 

The conf.json file needs to be filled with the missing data. Anyway the algorithm 
won't work correctly.

In order to make this algorithm work, you should have a table named ""

The R algorithm will generate two tables, a cleaned one for the 
user table and also another one for the objective table. This tables 
will have the ml_ preffix. 

Also to make the algorithm work you need to create two more tables. You need
to do this manually. These tables should also have the ml_ prefix, the 
table original name and Importance suffix. For example, if you have table named 
"students", the table should be named "ml_studentsImportance".

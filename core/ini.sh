#!/bin/bash 

# http://dailyraisin.com/read-json-value-in-bash/
function readJson {
  UNAMESTR=`uname`
  if [[ "$UNAMESTR" == 'Linux' ]]; then
    SED_EXTENDED='-r'
  elif [[ "$UNAMESTR" == 'Darwin' ]]; then
    SED_EXTENDED='-E'
  fi; 

  VALUE=`grep -m 1 "\"${2}\"" ${1} | sed ${SED_EXTENDED} 's/^ *//;s/.*: *"//;s/",?//'`

  if [ ! "$VALUE" ]; then
    echo "Error: Cannot find \"${2}\" in ${1}" >&2;
    exit 1;
  else
    echo $VALUE ;
  fi; 
}


host=`readJson conf.json host || exit 1`
databaseName=`readJson conf.json databaseName || exit 1`
userName=`readJson conf.json userName || exit 1`
password=`readJson conf.json password || exit 1`
objectiveTable=`readJson conf.json objectiveTable || exit 1`

echo "Hostname variable established to: $host"
echo "Database variable established to: $databaseName"
echo "Username variable established to: $userName"
echo "Password variable established to: $password"
echo "Objective table variable established to: $objectiveTable"
echo " ..... "

echo "Your configuration is correct. You can start developing your own project!"
